from django import forms

from .models import Category, Product


class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = '__all__'
        #fields = ['name', 'icon']


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = '__all__'
