from django.urls import path
from products import views

urlpatterns = [
    path('', views.homepage_view, name="homepage"),
    path('category_details/<int:pk>', views.CategoryDetailView.as_view(), name="category_details"),
    path('category_list/', views.CategoryListView.as_view(), name="category_list"),
    path('category_create/', views.CategoryCreateView.as_view(), name="category_create"),
    path('category_update/<int:pk>', views.CategoryUpdateView.as_view(), name="category_update"),
    path('category_delete/<int:pk>', views.CategoryDeleteView.as_view(), name="category_delete"),
    path('accounts/profile/', views.homepage_view, name="homepage"),
    path('product_list/', views.ProductListView.as_view(), name="product_list"),
    path('product_create/', views.ProductCreateView.as_view(), name="product_create"),
    path('product_update/<int:pk>', views.ProductUpdateView.as_view(), name="product_update"),
    path('product_delete/<int:pk>', views.ProductDeleteView.as_view(), name="product_delete"),
]
